""" Problem 2: Network Failure Point
Overview: We have a mesh network connected by routers labeled from
    1 to 6 in a directed manner. Write an algorithm that can detect
    the routers with the highest number of connections so we might
    know which routers will cause a network failure if removed.
    Multiple connections between the same routers should be treated
    as separate connections. A tie for the most number of connections
    is possible. 

Requirements: Implement a identify_router function that accepts an input
    graph of nodes representing the total network and identifies the node 
    with the most number of connections. 
    Return the label of the node. 
    Implement a directed graph data structure using Python 3.6 and up.
    Each node is unique thus there will be no cases of having multiple nodes
    with the same label.
    Each node will have an infinite number of both inbound and outbound links.
"""

""" Time Complexity
The time complexity if O(nm), because it iterates through every router in the
graph and then it iterates over all the outbound links of that router, hence
we get O(nm), and then it iterates through an array of six indices which is O(1).
And so, since O(nm) dominates, the amortized runtime would just be O(nm).
"""


from DirectedGraph import DirectedGraph
from GraphNode import GraphNode


def identify_router(network):
    link_count = [0,0,0,0,0,0]
    result = []
    for label in network:
        link_count[label - 1] += len(network[label])    # Add all the outbound links
        for outgoing in network[label]:
            link_count[outgoing - 1] += 1               # Add respective inbound links
    max_links = max(link_count)
    for index in range(6):
        if link_count[index] == max_links:              # Append the router with the most links
            result += [index + 1]
    return result


####### TEST CASES #######
network_TC1 = DirectedGraph()                   # Create graph for Test Case 1 (manually).
node1 = GraphNode(1)
node2 = GraphNode(2)
node3 = GraphNode(3)
node4 = GraphNode(4)
node5 = GraphNode(5)
node6 = GraphNode(6)

network_TC1.add_outbound(node1, node2)          # Add all nodes in the graph for Test Case 1.
network_TC1.add_outbound(node2, node3)
network_TC1.add_outbound(node3, node5)
network_TC1.add_outbound(node5, node2)
network_TC1.add_outbound(node2, node1)

network_TC2 = DirectedGraph([1,3,5,6,4,5,2,6])  # Create graph for Test Case 2 using array
network_TC3 = DirectedGraph([2,4,6,2,5,6])      # Create graph for Test Case 3 using array


print("Network Faliure Point for TC1: ", identify_router(network_TC1.get_graph()))
print("Network Faliure Point for TC2: ", identify_router(network_TC2.get_graph()))
print("Network Faliure Point for TC3: ", identify_router(network_TC3.get_graph()))