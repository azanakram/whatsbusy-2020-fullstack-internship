""" Problem 1: String Compression
Overview: Implement a string compression using python. For example,
    aaaabbbccddddddee would become a4b3c2d6e2. If the length
    of the string is not reduced, return the original string.

Requirements: Implement a compress function which accepts an input 
    string and returns a compressed string. Code must be implemented 
    using python 3.6 and must follow strictly pep8 rules.
    Provide comments regarding the implementation.
"""

""" Time Complexity
The time complexity of the function is O(n), as we only make 1 pass
through the provided string.
"""


def compress(s):
    if len(s) <= 1:
        return s

    letter = s[0]
    freq = 1
    compressedString = ""
    
    for i in s[1:]:
        if letter == i:
            freq += 1                                   # Count number of times the same letter repeats
        else:
            compressedString += letter + str(freq)      # Once you are at a different letter, concatenate
            letter = i
            freq = 1
    
    compressedString += letter + str(freq)

    return compressedString

    