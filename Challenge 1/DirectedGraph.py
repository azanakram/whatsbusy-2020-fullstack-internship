""" Class for Directed Graph
"""
from collections import defaultdict
from GraphNode import GraphNode


class DirectedGraph():
    def __init__(self, network=None):
        self.graph = defaultdict(list)
        if network:                                 # If initialized with an array
            network_length = len(network)
            if network_length == 1:                 # If the network consists of a single router
                self.add_node(GraphNode(network[0]))
            else:                                   # At least two routers are present
                router1 = 0
                router2 = 1
                while (router2 < network_length):
                    self.add_outbound(GraphNode(network[router1]), GraphNode(network[router2]))
                    router1 += 1
                    router2 += 1
    
    def add_node(self, node):
        if node.label not in self.graph:
            self.graph[node.label] = []

    def add_outbound(self, node1, node2):
        if node2.label not in self.graph[node1.label]:
            self.graph[node1.label].append(node2.label)

    def remove_node(self, node):
        if node.label in self.graph:
            self.graph.pop(node.label)
            for label in self.graph:
                if node.label in self.graph[label]:
                    self.graph[label].remove(node.label)

    def get_graph(self):
        return self.graph

